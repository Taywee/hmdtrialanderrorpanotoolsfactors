#version 130

uniform sampler2D tex;
uniform float a;
uniform float b;
uniform float c;
uniform float d;

// Position of the fragment in [-1, 1]
in vec2 frag_position;
in vec2 frag_uv;
out vec4 Out_Color;

void main() {
    float r_dest = length(frag_position);
    float r_src = a * pow(r_dest, 4) + b * pow(r_dest, 3) + c * pow(r_dest, 2) + d * r_dest;
    vec2 uv_src = (r_src * normalize(frag_position) + 1.0) / 2.0;
    Out_Color = texture(tex, uv_src);
}

