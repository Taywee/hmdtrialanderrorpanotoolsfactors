use axfive_imgui::events::Event;
use axfive_imgui::glium;
use axfive_imgui::{self as imgui, Context};
use glium::texture::RawImage2d;
use glium::uniform;
use glium::Surface;
use glium::uniforms::{MagnifySamplerFilter, MinifySamplerFilter, SamplerWrapFunction};
use std::error::Error;

macro_rules! static_cstr {
    ($string:literal) => {
        unsafe { std::ffi::CStr::from_bytes_with_nul_unchecked(concat!($string, '\0').as_bytes()) }
    };
}

#[derive(Copy, Clone)]
struct Vertex {
    // Position is also uv for this case
    position: [f32; 2],
    eye: u8,
}

glium::implement_vertex!(Vertex, position, eye);

fn main() -> Result<(), Box<dyn Error>> {
    let mut events_loop = glium::glutin::EventsLoop::new();
    let mut running = true;
    let wb = glium::glutin::WindowBuilder::new();
    let cb = glium::glutin::ContextBuilder::new();
    let mut display = glium::Display::new(wb, cb, &events_loop)?;

    // Generate the Dear ImGui context
    let mut context = Context::new(&display)?;

    let grid_image = image::load_from_memory(include_bytes!("images/grid.png"))?.into_rgb();

    let dimensions = grid_image.dimensions();

    let image = RawImage2d::from_raw_rgb(grid_image.into_raw(), dimensions);
    let texture = glium::Texture2d::new(&display, image)?;

    let vertex_shader_src = include_str!("vertex_shader.glsl");
    let fragment_shader_src = include_str!("fragment_shader.glsl");
    let program = glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None)?;

    let vertices = vec![
        Vertex {
            position: [-1.0, -1.0],
            eye: 0,
        },
        Vertex {
            position: [1.0, -1.0],
            eye: 0,
        },
        Vertex {
            position: [1.0, 1.0],
            eye: 0,
        },
        Vertex {
            position: [-1.0, 1.0],
            eye: 0,
        },
        Vertex {
            position: [-1.0, -1.0],
            eye: 1,
        },
        Vertex {
            position: [1.0, -1.0],
            eye: 1,
        },
        Vertex {
            position: [1.0, 1.0],
            eye: 1,
        },
        Vertex {
            position: [-1.0, 1.0],
            eye: 1,
        },
    ];
    let indices: Vec<u8> = vec![0, 1, 3, 1, 2, 3, 4, 5, 7, 5, 6, 7];

    let vertex_buffer = glium::VertexBuffer::immutable(&display, &vertices)?;
    let index_buffer = glium::IndexBuffer::immutable(
        &display,
        glium::index::PrimitiveType::TrianglesList,
        &indices,
    )?;

    let mut a = 0.0f32;
    let mut b = 0.0f32;
    let mut c = 0.0f32;
    let mut d = 1.0f32;

    while running {
        let events = context.start_frame(&mut events_loop, &mut display);
        for event in events {
            match event {
                Event::Quit => {
                    return Ok(());
                }
                _ => (),
            }
        }

        imgui::window(static_cstr!("Configure panotools parameters"), &mut running, 0, || {
            imgui::drag_float(static_cstr!("a"), &mut a, 0.001, 0.0, 0.0, static_cstr!("%.03f"), 1.0);
            imgui::drag_float(static_cstr!("b"), &mut b, 0.001, 0.0, 0.0, static_cstr!("%.03f"), 1.0);
            imgui::drag_float(static_cstr!("c"), &mut c, 0.001, 0.0, 0.0, static_cstr!("%.03f"), 1.0);
            imgui::drag_float(static_cstr!("d"), &mut d, 0.001, 0.0, 0.0, static_cstr!("%.03f"), 1.0);
        });

        if !running {
            return Ok(());
        }

        // Start actual rendering
        let mut frame = display.draw();

        frame.clear_color_and_depth((0.0, 0.0, 1.0, 1.0), 1.0);

        // Render texture bit
        frame.draw(&vertex_buffer, &index_buffer, &program, &uniform! {
            a: a,
            b: b,
            c: c,
            d: d,
            tex: texture.sampled().wrap_function(SamplerWrapFunction::Clamp).minify_filter(MinifySamplerFilter::Linear).magnify_filter(MagnifySamplerFilter::Linear),
        }, &glium::DrawParameters::default())?;

        // Render ImGui
        context.render(&mut display, &mut frame)?;
        frame.finish()?;
    }
    Ok(())
}
