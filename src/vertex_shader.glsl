#version 130

in vec2 position;
in uint eye;
out vec2 frag_position;
out vec2 frag_uv;

void main() {
    frag_position = position;
    frag_uv = (position + 1.0) / 2.0;
    if (eye == 0u) {
        gl_Position = vec4(position.x / 2.0 - 0.5, position.y, 0, 1);
    } else {
        gl_Position = vec4(position.x / 2.0 + 0.5, position.y, 0, 1);
    }
}

